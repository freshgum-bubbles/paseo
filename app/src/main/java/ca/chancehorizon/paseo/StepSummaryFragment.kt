package ca.chancehorizon.paseo


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_step_summary.*
import kotlinx.android.synthetic.main.fragment_step_summary.view.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


class StepSummaryFragment : androidx.fragment.app.Fragment(), OnChartValueSelectedListener {

    // the timeUnit variable allows for the same class to be used for
    //  fragments (screens) showing a summary for different time units
    var timeUnit: String = "???"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var running = false

    var targetDaySteps = 10000

    // target steps for months and years are calculated later based on actual number of days in
    //  the month (28 to 31) and year (365 or 366)
    //  all the calendar/time calculation would be so much simpler using metric time

    // receiver for step counting service
    var receiver: BroadcastReceiver? = null

    lateinit var paseoDBHelper : PaseoDBHelper

    lateinit var contextThemeWrapper : ContextThemeWrapper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // fill the screen with the latest steps data
        updateSummary()

        // set up the link between this screen and the step counting service
        configureReceiver()

        running = true
    }



    // set up receiving messages from the step counter service
    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateSummary()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment

        val theTheme = requireContext().theme
        contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view : View = inflater.inflate(R.layout.fragment_step_summary, container, false)

        // respond to the user tapping on the table button
        view.tableButton.setOnClickListener {
            showTable(true)
        }

        // respond to the user tapping on the graph button
        view.graphButton.setOnClickListener {
            showTable(false)
        }

        return view
    }



    override fun onDestroy() {
        super.onDestroy()

        context?.unregisterReceiver(receiver)

    }



    // needed for reacting to tapping on barchart (this is when tapping outside of the chart area)
    override fun onNothingSelected() {

    }



    // respond to user tapping on bar in barchart - show a "snackbar" with the number of steps for
    //  the time period of the bar that was tapped
    override fun onValueSelected(theBar: Entry?, h: Highlight?) {
        val snack = Snackbar.make(requireView(), NumberFormat.getIntegerInstance().format(theBar?.y) + " steps", Snackbar.LENGTH_SHORT)
        snack.view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.grey_900))
        val textView = snack.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grey_50))
        snack.show()
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()
            updateSummary()
    }



    // toggle between showing the steps table and showing the steps graph
    fun showTable(showTheTable : Boolean) {

        // show the table and hid the graph (also reformat the buttons accordingly)
        if (showTheTable && stepsTable.visibility == View.GONE) {
            stepsTable.visibility = View.VISIBLE
            stepsGraph.visibility = View.GONE

            tableButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorPrimary))
            tableButton.setBackgroundResource(R.drawable.bottom_border_on)
            graphButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorSecondary))
            graphButton.setBackgroundResource(R.drawable.bottom_border)
        }
        // hide the table and show the graph (also reformat the buttons accordingly)
        else if (!showTheTable && stepsTable.visibility == View.VISIBLE) {
            stepsTable.visibility = View.GONE
            stepsGraph.visibility = View.VISIBLE

            tableButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorSecondary))
            tableButton.setBackgroundResource(R.drawable.bottom_border)
            graphButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorPrimary))
            graphButton.setBackgroundResource(R.drawable.bottom_border_on)
        }
    }



    // fill in the steps data on the screen based on the time unit selected by the user (from the menu)
    fun updateSummary() {
        if (running && timeUnit != "miniGoals")
        {
            var theDateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()) // looks like "19891225"
            val date = theDateFormat.format(Date()).toInt()
            theDateFormat = SimpleDateFormat("mm", Locale.getDefault()) // looks like 00, 01... 58, 59
            val minute = max(theDateFormat.format(Date()).toInt(), 1) // value needs to be at least 1 in order to prevent divide by zero error
            theDateFormat = SimpleDateFormat("HH", Locale.getDefault()) // looks like 00, 01... 23, 24
            val hour = max(theDateFormat.format(Date()).toInt(), 1) // value needs to be at least 1 in order to prevent divide by zero error

            val dayOfWeek = getDayOfWeekNumber(Date())

            val calendar = Calendar.getInstance()

            calendar.time = Date()
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) // the day of the month in numerical format
            val dayOfYear = calendar.get(Calendar.DAY_OF_YEAR) // the day of the year in numerical format

            var theSteps = 0
            var projectedSteps = 0

            // get the application settings (save messages etc)
            val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
            targetDaySteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()
            // default to Monday as first day of week
            val weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

            var targetSteps = targetDaySteps // to be overriden later for differnt time units
            val stepsArray: ArrayList<Array<Int>>
            var projectedProgress = 0
            var expectedProgress = 0

            var datePrefix = " "
            var theDate = Date()

            // if the time unit was not set when calling this fragment, read it from share preferences
            if (timeUnit == "???") {
                timeUnit = paseoPrefs.getString("prefLastTimeUnit", "days")!!
            }
            // set basic parameters based on time unit summary being assembled.
            when (timeUnit) {
                "hours" -> {
                    targetSteps = targetDaySteps / 10
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy, HH:00", Locale.getDefault()) // looks like "Dec 25, 1989  13:00"
                    theSteps = paseoDBHelper.getSteps("hours", hour, date)
                    projectedSteps = theSteps * 60 / minute
                    projectedProgress = min(100 * theSteps * 60 / minute / targetSteps, 100)
                    expectedProgress = 100 * minute / 60
                }
                "days" -> {
                    targetSteps = targetDaySteps
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
                    theSteps = paseoDBHelper.getDaysSteps(date)
                    projectedSteps = theSteps * 24 / hour
                    projectedProgress = min(100 * theSteps * 24 / hour / targetSteps, 100)
                    expectedProgress = 100 * hour / 24
                }
                "weeks" -> {
                    targetSteps = targetDaySteps * 7
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
                    theSteps = paseoDBHelper.getSteps("weeks", date, 0, weekStart)
                    projectedSteps = theSteps * 7 / dayOfWeek
                    projectedProgress = min(100 * theSteps * 7 / dayOfWeek / targetSteps, 100)
                    expectedProgress = 100 * dayOfWeek / 7
                    datePrefix = "Week of "
                    theDate = getFirstDayInWeek()
                }
                "months" -> {
                    theDateFormat = SimpleDateFormat("MMMM", Locale.getDefault()) // looks like "December"
                    theSteps = paseoDBHelper.getSteps("months", date)
                    val daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
                    targetSteps = targetDaySteps * daysInMonth
                    projectedSteps = theSteps * daysInMonth / dayOfMonth
                    projectedProgress = min(100 * theSteps * daysInMonth / dayOfMonth / targetSteps, 100)
                    expectedProgress = 100 * dayOfMonth / daysInMonth
                }
                "years" -> {
                    theDateFormat = SimpleDateFormat("yyyy", Locale.getDefault()) // looks like "1989"
                    theSteps = paseoDBHelper.getSteps("years", date)
                    val daysInYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
                    targetSteps = targetDaySteps * daysInYear
                    projectedSteps = theSteps * (daysInYear / dayOfYear)
                    projectedProgress = min(100 * theSteps * (daysInYear / dayOfYear) / targetSteps, 100)
                    expectedProgress = 100 * dayOfYear / daysInYear
                }
            }
            var theText = datePrefix + theDateFormat.format(theDate)
            dateValue.text = theText

            // get steps for every time unit from the steps table in the database
            stepsArray = paseoDBHelper.getStepsByTimeUnit(date, timeUnit, true, weekStart)

            // only fill in the steps table and steps graph if steps have been recorded
            //  (otherwise get an error, when trying to display graph the first time Paseo is run)
            if (stepsArray.size > 0 ) {

                createStepsTable(stepsArray)

                // display today's steps on the main screen
                theText = NumberFormat.getIntegerInstance().format(theSteps)
                stepsValue.text = theText
                stepsProgressBar.max = targetSteps // need to set/save target steps in preferences
                stepsProgressBar.progress = theSteps
                stepsProgressBar.secondaryProgress = targetSteps
                theText = (theSteps.toDouble() / targetSteps.toDouble() * 100.0).roundToInt().toString() + "% of target (" +
                        NumberFormat.getIntegerInstance().format(targetSteps) + ")"
                percentTargetValue.text = theText

                // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
                //  the daily target steps
                expectedStepsProgressBar.max = 100
                expectedStepsProgressBar.progress = expectedProgress

                // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
                //  the daily target steps
                projectedStepsProgressBar.max = 100
                try {
                    projectedStepsProgressBar.progress = projectedProgress
                } catch (e: SQLiteException) {
                    projectedStepsProgressBar.progress = 0
                }

                // get this hour's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("hours", hour, date)
                // display this hour's steps on the main screen
                hourStepsLabel.text = (getString(R.string.steps_this_hour))
                hourStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this week's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("days", date)
                // display this hour's steps on the main screen
                dayStepsLabel.text = (getString(R.string.steps_this_day))
                dayStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this week's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("weeks", date, 0, weekStart)
                // display this hour's steps on the main screen
                weekStepsLabel.text = (getString(R.string.steps_this_week))
                weekStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this month's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("months", date)
                // display this month's steps on the main screen
                monthStepsLabel.text = (getString(R.string.steps_this_month) )
                monthStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this month's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("years", date)
                // display this month's steps on the main screen
                yearStepsLabel.text = (getString(R.string.steps_this_year))
                yearStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                createStepsGraph(stepsArray, projectedSteps, targetSteps)

                showTable(false)
            }
        }
    }



    fun createStepsTable(stepsArray: ArrayList<Array<Int>>){
        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // default to Monday as first day of week
        val weekStart = paseoPrefs?.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())?.toInt()

        stepsTable.removeAllViews()

        val headerRow = TableRow(context)
        headerRow.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)

        val hourTitleCell = TextView(context)
        hourTitleCell.apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT)
            text = timeUnit.capitalize()
        }
        headerRow.addView(hourTitleCell)

        val stepsTitleCell = TextView(context)
        stepsTitleCell.apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT)
            text = getString(R.string.steps)
        }
        headerRow.addView(stepsTitleCell)

        stepsTable.addView(headerRow)

        // add a row in the table for each unit of time so far
        //  *** need to paginate the table
        for (stepTime in 0 until stepsArray.size) {

            // skip the record if it is essentially empty (all zeros)
            if (stepsArray[stepTime][0] == 0 && stepsArray[stepTime][1] == 0)
                continue

            val row = TableRow(context)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)

            // set up the time value for the time cell in the table
            val timeCell = TextView(context)
            timeCell.apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT)

                var theText = ""

                // depending on the type of time unit, format the entry in the first column
                //  (the time/date of the number of steps)
                when (timeUnit) {
                    "hours" -> {
                        theText = stepsArray[stepTime][0].toString() + "  "
                    }
                    "days" -> {
                        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                        val cal = Calendar.getInstance()
                        cal.set(Calendar.DAY_OF_MONTH, stepsArray[stepTime][0].toString().substring(6,8).toInt())
                        cal.set(Calendar.MONTH, stepsArray[stepTime][0].toString().substring(4,6).toInt() - 1)
                        cal.set(Calendar.YEAR, stepsArray[stepTime][0].toString().substring(0,4).toInt())
                        theText = sdf.format(cal.time) + "   "
                    }
                    "weeks" -> {
                        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                        val cal = Calendar.getInstance()
                        cal.set(Calendar.YEAR, stepsArray[stepTime][2])
                        cal.set(Calendar.WEEK_OF_YEAR, stepsArray[stepTime][0])
                        cal.minimalDaysInFirstWeek = 6
                        if (weekStart == 2) {
                            cal.firstDayOfWeek = 1
                            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                        }
                        else {
                            cal.firstDayOfWeek = 0
                            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                        }
                        theText = sdf.format(cal.time) + "   "
                    }
                    "months" -> {
                        val sdf = SimpleDateFormat("MMMM, yyyy", Locale.getDefault())
                        val cal = Calendar.getInstance()
                        cal.set(Calendar.YEAR, stepsArray[stepTime][2])
                        cal.set(Calendar.MONTH, stepsArray[stepTime][0] - 1)
                        theText = sdf.format(cal.time) + "   "
                    }
                    "years" -> {
                        val sdf = SimpleDateFormat("yyyy", Locale.getDefault())
                        val cal = Calendar.getInstance()
                        cal.set(Calendar.YEAR, stepsArray[stepTime][0])
                        theText = sdf.format(cal.time) + "   "
                    }
                }
                text = theText
            }
            row.addView(timeCell)

            // enter the number of steps for this unit of time in the table
            val stepsCell = TextView(context)
            stepsCell.apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT)
                text = stepsArray[stepTime][1].toString()
            }
            row.addView(stepsCell)

            stepsTable.addView(row, 1)
        }
    }



    fun createStepsGraph (stepsArray: ArrayList<Array<Int>>, projectedSteps: Int, targetSteps: Int) {
        // begin setting up the graph
        stepsGraph.setOnChartValueSelectedListener(this)

        stepsGraph.setDrawBarShadow(false)
        stepsGraph.setDrawValueAboveBar(false)
        stepsGraph.setDrawValueAboveBar(false)

        stepsGraph.description.isEnabled = false

        // if more than 366 entries are displayed in the chart, no values will be
        // drawn
        stepsGraph.setMaxVisibleValueCount(366)

        // scaling done on x- and y-axis separately
        stepsGraph.setPinchZoom(false)

        stepsGraph.setDrawGridBackground(false)

        val xAxis = stepsGraph.xAxis
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // show an x axis label for a maximum of every bar
        xAxis.labelCount = 7 // show a maximum of 7 x axis labels (skipping labels if more than 7 bars)
        xAxis.setCenterAxisLabels(false)
        xAxis.setDrawGridLines(false)
        xAxis.labelRotationAngle = -45f

        val leftAxis = stepsGraph.axisLeft
        leftAxis.setLabelCount(8, false)
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f

        val rightAxis = stepsGraph.axisRight
        rightAxis.setDrawGridLines(false)
        rightAxis.setLabelCount(8, false)
        rightAxis.spaceTop = 15f
        rightAxis.axisMinimum = 0f

        val graphLegend = stepsGraph.legend
        graphLegend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        graphLegend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graphLegend.orientation = Legend.LegendOrientation.HORIZONTAL
        graphLegend.setDrawInside(false)
        graphLegend.form = LegendForm.SQUARE
        graphLegend.formSize = 9f
        graphLegend.textSize = 11f
        graphLegend.xEntrySpace = 4f

        val entries = ArrayList<BarEntry>()

        var maxBarsToShow = 10
        var maxXVal = 0
        var newXVal = 0
        // save the first time/date value so that the graph and x axis labels start at the same place and are aligned
        var minXVal = stepsArray[0][0]
        val xAxisLabels = ArrayList<String>()

        // calendar instance to be used to create x axis labels
        val cal = Calendar.getInstance()

        // loop through each time unit to create the steps by time/date bar graph
        for (stepTime in 0 until stepsArray.size) {

            if (timeUnit == "weeks")
            {
                minXVal = 0
                maxXVal = stepTime
            }
            else {
                maxXVal = stepsArray[stepTime][0]
            }

            var xVal = maxXVal - minXVal

            if (timeUnit == "days" || timeUnit == "weeks" ||timeUnit == "months") {
                xVal = stepTime
                newXVal = maxXVal
            }

            if (newXVal == 0) {
                newXVal = maxXVal
            }

            // set the y value for the bar
            var stackSteps = 0f
            if (stepTime == stepsArray.size - 1) {
                stackSteps = projectedSteps - stepsArray[stepTime][1].toFloat()
            }

            entries.add(BarEntry(xVal.toFloat(), floatArrayOf(stepsArray[stepTime][1].toFloat(),
                    stackSteps)))

            // set the x label for the bar (need to loop through several bars if zero steps taken in time period)
            while (newXVal <= maxXVal) {
                // depending on the type of time unit, set the tick labels for the x axis
                when (timeUnit) {
                    "hours" -> {
                        xAxisLabels.add(newXVal.toString() + ":00  ")
                        //xAxisLabels.add(stepsArray[stepTime][0].toString() +":00  ")
                    }
                    "days" -> {
                        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                        cal.set(Calendar.YEAR, newXVal.toString().substring(0, 4).toInt())
                        cal.set(Calendar.MONTH, newXVal.toString().substring(4, 6).toInt() - 1)
                        cal.set(Calendar.DAY_OF_MONTH, newXVal.toString().substring(6, 8).toInt())
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "weeks" -> {
                        val sdf = SimpleDateFormat("MMM", Locale.getDefault())
                        cal.set(Calendar.WEEK_OF_YEAR, stepsArray[stepTime][0] + 1)
                        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "months" -> {
                        val sdf = SimpleDateFormat("MMMM", Locale.getDefault())
                        cal.set(Calendar.MONTH, newXVal - 1)
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "years" -> {
                        val sdf = SimpleDateFormat("yyyy", Locale.getDefault())
                        cal.set(Calendar.YEAR, newXVal)
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                }
                newXVal = newXVal + 1
            }
        }
        // apply the formatted x axis tick labels
        xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabels)

        // create the graph
        val set = BarDataSet(entries, "Steps")
        set.stackLabels = arrayOf("Steps", "Projected")

        // do not draw labels on the bars
        set.setDrawValues(false)

        // use the main color from the current theme for the bars in the chart
        val theColor = requireContext().getThemeColor(R.attr.colorPrimary)
        val theColor2 = requireContext().getThemeColor(R.attr.colorAccent)
        set.setColors(theColor, theColor2)

        val data = BarData(set)
        stepsGraph.data = data

        // set the graph colors to match the current theme
        @ColorInt val theChartTextColor = resolveColorAttr(requireView().context,
                android.R.attr.textColorPrimary)
        stepsGraph.setBorderColor(theChartTextColor)
        stepsGraph.xAxis.textColor = theChartTextColor
        stepsGraph.xAxis.gridColor = theChartTextColor
        stepsGraph.xAxis.axisLineColor = theChartTextColor
        stepsGraph.axisRight.textColor = theChartTextColor
        stepsGraph.axisLeft.textColor = theChartTextColor
        stepsGraph.legend.textColor = (theChartTextColor)

        val maxSteps = paseoDBHelper.getMaxSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
        val minSteps = paseoDBHelper.getMinSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
        val averageSteps = paseoDBHelper.getAverageSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())

        // add a horizontal line at the target steps
        val targetLimitLine = LimitLine(targetSteps.toFloat(), "Target")
        targetLimitLine.lineWidth = 1f
        targetLimitLine.enableDashedLine(10f, 10f, 0f)
        targetLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        targetLimitLine.textSize = 10f
        targetLimitLine.lineColor = theChartTextColor
        targetLimitLine.textColor = theChartTextColor

        // add a horizontal line at the maximum steps
        val maxLimitLine = LimitLine(maxSteps.toFloat(), "Max")
        maxLimitLine.lineWidth = 1f
        maxLimitLine.enableDashedLine(10f, 10f, 0f)
        maxLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        maxLimitLine.textSize = 10f
        maxLimitLine.lineColor = theChartTextColor
        maxLimitLine.textColor = theChartTextColor

        // do not show max, min and average limit lines for hours summary
        if (timeUnit != "hours") {
            // add a horizontal line at the minimum steps
            val minLimitLine = LimitLine(minSteps.toFloat(), "Min")
            minLimitLine.lineWidth = 1f
            minLimitLine.enableDashedLine(10f, 10f, 0f)
            minLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            minLimitLine.textSize = 10f
            minLimitLine.lineColor = theChartTextColor
            minLimitLine.textColor = theChartTextColor

            // add a horizontal line at the minimum steps
            val avgLimitLine = LimitLine(averageSteps.toFloat(), "Avg")
            avgLimitLine.lineWidth = 1f
            avgLimitLine.enableDashedLine(10f, 10f, 0f)
            avgLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            avgLimitLine.textSize = 10f
            avgLimitLine.lineColor = theChartTextColor
            avgLimitLine.textColor = theChartTextColor


            stepsGraph.axisLeft.removeAllLimitLines() // reset all limit lines to avoid overlapping lines
            stepsGraph.axisLeft.addLimitLine(maxLimitLine)
            stepsGraph.axisLeft.addLimitLine(avgLimitLine)
            stepsGraph.axisLeft.addLimitLine(targetLimitLine)
            stepsGraph.axisLeft.addLimitLine(minLimitLine)
        }

        // depending on the type of time unit, set the tick labels for the x axis
        when (timeUnit) {
            "hours" -> {
                maxBarsToShow = 24
            }
            "days" -> {
                maxBarsToShow = 10
            }
            "weeks" -> {
                maxBarsToShow = 10
            }
            "months" -> {
                maxBarsToShow = 12
            }
            "years" -> {
                maxBarsToShow = 10
            }
        }

        // set the initial zoom level of the chart
        stepsGraph.setVisibleXRangeMaximum(maxBarsToShow.toFloat())

        // move the chart to show only the right most bars (user can swipe or pinch to show more bars)
        stepsGraph.moveViewToX(maxXVal.toFloat())
        stepsGraph.invalidate() // refresh

        // override the maximum visible range so that the user can pinch to zoom out
        stepsGraph.setVisibleXRangeMaximum(366f)
        stepsGraph.minimumHeight = 800
    }



    // get the day of week number of the first day of week based on the currently set Locale of the device
    fun getLocaleFirstDayOfWeek(): Int {
        val theCalendar = Calendar.getInstance(Locale.getDefault())
        return theCalendar.firstDayOfWeek
    }



    // return the date of the first day in the current week (could be the most recent Sunday or Monday)
    fun getFirstDayInWeek(): Date {

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // default to Monday as first day of week
        val weekStart = paseoPrefs?.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())?.toInt()

        var theCalendar = Calendar.getInstance(Locale.UK)

        // use a CANADA calendar so that start of week is Sunday
        if (weekStart == 1) {
            theCalendar = Calendar.getInstance(Locale.CANADA)
        }

        theCalendar[Calendar.HOUR_OF_DAY] = 0 // ! clear would not reset the hour of day !

        theCalendar.clear(Calendar.MINUTE)
        theCalendar.clear(Calendar.SECOND)
        theCalendar.clear(Calendar.MILLISECOND)

        // get start of this week in milliseconds
        theCalendar[Calendar.DAY_OF_WEEK] = theCalendar.firstDayOfWeek

        return theCalendar.time
    }



    fun getDayOfWeekNumber(date: Date?): Int {

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val weekStart = paseoPrefs!!.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

        // default to Sunday as first day of week
        val theCalendar = Calendar.getInstance()
        theCalendar.time = date
        var dayOfWeek = theCalendar[Calendar.DAY_OF_WEEK]

        // set first day of week to Monday if set by user (in app settings)
        if (weekStart == 2) {
            dayOfWeek = dayOfWeek - 1
        }

        return dayOfWeek
    }



    // get the color set by the current theme for specific color references
    fun Context.getThemeColor(
            @AttrRes attrColor: Int,
            typedValue: TypedValue = TypedValue(),
            resolveRefs: Boolean = true
            ): Int {
        theme.resolveAttribute(attrColor, typedValue, resolveRefs)
        return typedValue.data
    }



    // get theme details
    //  used by getThemeColor
    fun resolveThemeAttr(context: Context, @AttrRes attrRes: Int): TypedValue {
        val theme = context.theme
        val typedValue = TypedValue()
        theme.resolveAttribute(attrRes, typedValue, true)
        return typedValue
    }



    @ColorInt
    fun resolveColorAttr(context: Context, @AttrRes colorAttr: Int): Int {
        val resolvedAttr = resolveThemeAttr(context, colorAttr)
        // resourceId is used if it's a ColorStateList, and data if it's a color reference or a hex color
        val colorRes = if (resolvedAttr.resourceId != 0)
            resolvedAttr.resourceId
        else
            resolvedAttr.data
        return ContextCompat.getColor(context, colorRes)
    }
}