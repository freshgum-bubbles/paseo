package ca.chancehorizon.paseo


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.preference.DropDownPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.io.File
import java.util.*


class PrefsFragment : PreferenceFragmentCompat(), TextToSpeech.OnInitListener {

    private var tts: TextToSpeech? = null
    private var ttsAvailable = false

    private val TAG = "Paseo Permission Request"
    private val WRITE_REQUEST_CODE = 102
    private val READ_REQUEST_CODE = 103

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tts = TextToSpeech(getContext(), this)

        val paseoPrefs = getContext()?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        ttsAvailable = paseoPrefs!!.getBoolean("prefTTSAvailable", false)

        if (ttsAvailable) {
            val result = tts?.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language not supported")
            }
            val voiceLanguages : DropDownPreference? = findPreference("prefVoiceLanguage")

            // only populate the list of voices if tts is available on the device
            if (tts != null && voiceLanguages != null) {
                setListPreferenceData(voiceLanguages)
            }
        }
        else {
            Log.e("TTS", "Initialization failed")

            // disable all the settings for text to speech
            val ttsItems : PreferenceCategory? = findPreference("textToSpeech")
            ttsItems?.isEnabled = false

            ttsItems?.setSummary(getString(R.string.no_TTS))

        }
    }



    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.paseo_settings, rootKey)
    }



    override fun onInit(status: Int) {

        if (status == TextToSpeech.SUCCESS) {
            val result = tts?.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language not supported")
            }
            val voiceLanguages : DropDownPreference? = findPreference("prefVoiceLanguage")

            // only populate the list of voices if tts is available on the device
            if (tts != null) {
                setListPreferenceData(voiceLanguages!!)
            }
        }
    }



    override fun onDestroy() {

        // Shutdown TTS
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }

        super.onDestroy()
    }



    // populate the list of available languages for text to speech
    fun setListPreferenceData(ttsLanguages : DropDownPreference) {

        // only populate the list of voice if tts is available on the device
        if (tts == null || tts!!.voice == null) {
            return
        }

        val availableVoices = tts!!.voices
        val availableLocales:List<Locale> = Locale.getAvailableLocales().toList()

        val voiceList = mutableListOf<String>()

        // loop through all the voices and create a list of them
        for (v: Voice in availableVoices){
            if (v.locale.language == Locale.getDefault().language
                    && availableLocales.contains(v.locale)
                    && !v.isNetworkConnectionRequired
                    && tts?.isLanguageAvailable(v.locale) != TextToSpeech.LANG_MISSING_DATA
                    && tts?.isLanguageAvailable(v.locale) != TextToSpeech.LANG_NOT_SUPPORTED
                    && !(v.features.contains(TextToSpeech.Engine.KEY_FEATURE_NOT_INSTALLED))){
                voiceList.add(v.getName())
            }
        }

        // sort the voices list so that similar voices (by country) are listed together
        voiceList.sort()

        val voiceArray = voiceList.toTypedArray()

        ttsLanguages.setEntries(voiceArray)

        val paseoPrefs = getContext()?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val ttsVoice = paseoPrefs!!.getString("prefVoiceLanguage", "en-US-language")
        val savedVoice = voiceArray.indexOf(ttsVoice)

        ttsLanguages.setDefaultValue(ttsVoice)

        ttsLanguages.setEntryValues(voiceArray)
    }



    // respond to the user tapping on specific preferences
    override fun onPreferenceTreeClick(preference: Preference?): Boolean {

        val key = preference!!.key

        // play a test message to test if voice settings are suitable
        if (key == "prefTestVoiceButton") {

            if (!ttsAvailable) {
                Toast.makeText(context,"No text to speech found on this device", Toast.LENGTH_LONG).show()
            }
            else {
                    val theText = "10000 steps."
                val paseoPrefs =
                    getContext()?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

                val ttsPitch = paseoPrefs!!.getFloat("prefVoicePitch", 100F)
                val ttsRate = paseoPrefs.getFloat("prefVoiceRate", 100F)

                // set the voice to use to speak with
                val ttsVoice = paseoPrefs.getString("prefVoiceLanguage", "en_US - en-US-language")
                val ttsLocale1 = ttsVoice?.substring(0, 2)
                val ttsLocale2 = ttsVoice?.substring(3)
                val voiceobj = Voice(ttsVoice, Locale(ttsLocale1, ttsLocale2), 1, 1, false, null)
                tts?.setVoice(voiceobj)

                tts?.setPitch(ttsPitch / 100)
                tts?.setSpeechRate(ttsRate / 100)

                var attemptSpeech = tts?.speak(theText, TextToSpeech.QUEUE_FLUSH, null, "")

                if (attemptSpeech == -1) {
                    tts = TextToSpeech(getContext(), this)

                    attemptSpeech = tts?.speak(theText, TextToSpeech.QUEUE_FLUSH, null, "")
                }

            }
        }
        // Export
        if (key == "prefExport") {
            val permission = ContextCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.i("TAG", "Permission to write denied")

                if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.setMessage(R.string.write_permission_last_warning)
                            .setTitle(R.string.permission_warning_title)

                    builder.setPositiveButton("OK") { dialog, id ->
                        ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_REQUEST_CODE)
                    }

                    val dialog = builder.create()
                    dialog.show()
                } else {
                    ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_REQUEST_CODE)
                }
            }
            else {
                val dbFile = File("/data/data/ca.chancehorizon.paseo/databases/paseoDB.db")
                if (dbFile.exists()) {
                    File("/storage/emulated/0/Download/paseoDB.db").delete()
                    dbFile.copyTo(File("/storage/emulated/0/Download/paseoDB.db"), overwrite = true)
                    dbFile.copyTo(File("/storage/emulated/0/Download/paseoDB.db"), overwrite = true)
                    Toast.makeText(context,getText(R.string.exportedDetails), Toast.LENGTH_LONG).show()
                }
            }
        }

        // Import
        if (key == "prefImport") {
            val permission = ContextCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                Log.i("TAG", "Permission to write denied")

                if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.setMessage(R.string.read_permission_last_warning)
                        .setTitle(R.string.permission_warning_title)

                    builder.setPositiveButton("OK") { dialog, id ->
                        ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), READ_REQUEST_CODE)
                    }

                    val dialog = builder.create()
                    dialog.show()
                } else {
                    ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), READ_REQUEST_CODE)
                }
            }
            else {
                // set up the path to this application's files

                // set up the path to this application's files
                val appFilesDir = getExternalStoragePublicDirectory("goo")

                val dbFile = File("/storage/emulated/0/Download/paseoDB.db")
                if (dbFile.exists()) {
                    try {
                        dbFile.copyTo(File("/data/data/ca.chancehorizon.paseo/databases/paseoDB.db"), overwrite = true)
                    }
                    catch (e: Error){
                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                    }

                    Toast.makeText(context, getText(R.string.importedDetails), Toast.LENGTH_LONG).show()
                }
                else {
                    Toast.makeText(context, getText(R.string.importedFailed), Toast.LENGTH_LONG).show()
                }
            }
        }

        // show the paseo about bottomsheet (which is also shown on "first run"
        if (key == "prefAbout") { // do your work
            val view: View = layoutInflater.inflate(R.layout.paseo_welcome_bottomsheet, null)

            val aboutPaseoBottomSheet = BottomSheetDialog(requireContext())
            aboutPaseoBottomSheet.setContentView(view)
            aboutPaseoBottomSheet.show()
            val bottomSheet = aboutPaseoBottomSheet.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        // show the paseo license bottomsheet
        if (key == "prefLicense") {
            val view: View = layoutInflater.inflate(R.layout.paseo_license_bottomsheet, null)

            val aboutPaseoBottomSheet = BottomSheetDialog(requireContext())
            aboutPaseoBottomSheet.setContentView(view)
            aboutPaseoBottomSheet.show()
            val bottomSheet = aboutPaseoBottomSheet.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        return super.onPreferenceTreeClick(preference)
    }



    override fun onDisplayPreferenceDialog(preference: Preference?) {
        val theDialog = preference as? OptionDialogPreference

        if (theDialog != null) {
            val dialogFragment = DialogPrefCompat.newInstance(theDialog.key)

            dialogFragment.setTargetFragment(this, 0)

            dialogFragment.positiveResult = {}

            val theFragmentManager = requireFragmentManager()
            dialogFragment.show(theFragmentManager, null)
        }
        else
        {
            super.onDisplayPreferenceDialog(preference)
        }
    }
}