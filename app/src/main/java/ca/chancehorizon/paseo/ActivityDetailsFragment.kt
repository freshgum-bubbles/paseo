package ca.chancehorizon.paseo


import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_details_bottomsheet.*


class ActivityDetailsFragment : BottomSheetDialogFragment(), View.OnClickListener {

    var activityType: String = "walking"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var date: String = "day"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var dateInt: Int = 20201225
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var startTime: String = "13:15:22"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var startTimeInt: Int = 131522
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var duration: String = "00:22:56"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var distance: String = " -- km"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var steps: Int = 5000
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var notes: String = "relaxing walk"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    // flags whether the user has edited the recorded activity
    var editsMade = false

    lateinit var paseoDBHelper : PaseoDBHelper


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val theTheme = requireContext().getTheme()
        val contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        return inflater.cloneInContext(contextThemeWrapper).inflate(R.layout.activity_details_bottomsheet, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // fill in the details for this activity
        updateDetail()
    }

//    override fun onAttach(context: Context?) {
//        super.onAttach(context)date.replace(":", "").toInt()
//    }



    override fun onDestroyView() {

        // save edits to database and update the activities list when the bottom sheet is closed
        if (editsMade ||notesValue.text.toString() != notes ) {

            notes = notesValue.text.toString()

            // save changes to database
            paseoDBHelper.updateActivity(dateInt, startTimeInt, activityType, notes)

            // update the activities list
            val pf = this.parentFragment as androidx.fragment.app.Fragment?
            pf?.onResume()
        }

        super.onDestroyView()
    }



    override fun onDetach() {

        super.onDetach()
    }



    fun updateDetail() {

        activityDate.text = date

        when(activityType) {
            "walking" -> {activityIcon.setImageResource(R.drawable.ic_walk)}
            "bicycling" -> {activityIcon.setImageResource(R.drawable.ic_bicycle)}
            "running" -> {activityIcon.setImageResource(R.drawable.ic_run)}
        }

        // respond to user tapping on activity icon
        activityIcon.setOnClickListener{ view ->
            val activitiesMenu: PopupMenu = PopupMenu(context, activityIcon)
            activitiesMenu.menuInflater.inflate(R.menu.activities_menu, activitiesMenu.menu)

            for (i in 0 until activitiesMenu.menu.size()) {
                val drawable: Drawable = activitiesMenu.menu.getItem(i).getIcon()
                if (drawable != null) {
                    drawable.mutate()

                    // make sure the icons color matches the current theme
                    drawable.setColorFilter(resolveColorAttr(requireView().context, android.R.attr.colorPrimary), PorterDuff.Mode.SRC_ATOP)
                }
            }

            activitiesMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.walk_item -> {
                        activityType = "walking"
                        activityIcon.setImageResource(R.drawable.ic_walk)
                    }
                    R.id.run_item -> {
                        activityType = "running"
                        activityIcon.setImageResource(R.drawable.ic_run)
                    }
                    R.id.bicycle_item -> {
                        activityType = "bicycling"
                        activityIcon.setImageResource(R.drawable.ic_bicycle)
                    }
                }

                // flag that user has edited this record (so that the activities list can show the edits made)
                editsMade = true

                true
            })


            try {
                val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible = true
                val mPopup = fieldMPopup.get(activitiesMenu)
                mPopup.javaClass
                        .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                        .invoke(mPopup, true)
            }
            catch (e: Exception){
                //Log.e("Main", "Error showing menu icons.", e)
            }
            finally {
                activitiesMenu.show()
            }
            //activitiesMenu.show()
        }

        startTimeValue.text = startTime
        durationValue.text = duration
        distanceValue.text = distance
        stepsValue.text = steps.toString()
        notesValue.setText(notes)
    }



    // respond to user taps
    override fun onClick(view: View) {
        // close the dialog
        // dismiss()
    }



    fun resolveThemeAttr(context: Context, @AttrRes attrRes: Int): TypedValue {
        val theme = context.theme
        val typedValue = TypedValue()
        theme.resolveAttribute(attrRes, typedValue, true)
        return typedValue
    }



    @ColorInt
    fun resolveColorAttr(context: Context, @AttrRes colorAttr: Int): Int {
        val resolvedAttr = resolveThemeAttr(context, colorAttr)
        // resourceId is used if it's a ColorStateList, and data if it's a color reference or a hex color
        val colorRes = if (resolvedAttr.resourceId != 0)
            resolvedAttr.resourceId
        else
            resolvedAttr.data
        return ContextCompat.getColor(context, colorRes)
    }

}