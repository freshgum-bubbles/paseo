package ca.chancehorizon.paseo



import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


class DashboardFragment : androidx.fragment.app.Fragment() {

    var running = false
    var startSteps = 0

    // default values for target steps (overridden later from shared preferences)
    var targetSteps = 10000
    var targetWeekSteps = targetSteps * 7
    // typically there are thirty days in a month.  This will be overridden later based on actual days in the month
    val daysInMonth = getDaysInMonth(-1)
    val targetMonthSteps = targetSteps * daysInMonth
    val daysInYear = if (isLeapYear(Calendar.getInstance().get(Calendar.YEAR))) 366 else 365
    var targetYearSteps = targetSteps * daysInYear

    // default the starting day of week to Monday (overridden later from saved settings)
    var weekStart = 2

    var lastStepDate = 0

    var receiver: BroadcastReceiver? = null

    lateinit var paseoDBHelper : PaseoDBHelper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // get the date of the last record from the steps table in the database
        lastStepDate = paseoDBHelper.readLastStepsDate()

        // get the start steps of the last record from the steps table in the database
        startSteps = paseoDBHelper.readLastStartSteps()

        startSteps = startSteps + 0

        updateDashboard()

        configureReceiver()

        running = true

    }



    // set up receiving messages from the step counter service
    private fun configureReceiver()
    {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateDashboard()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_dashboard, container, false)

        // respond to the user tapping on the dashboard legend
        //  (essentially making the legend into a button)
        view.legendLayout.setOnClickListener { _ ->
            // show the dashboard legend detailed descriptions dialog sheet
            val bottomSheet = DashboardLegendFragment()
            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        targetSteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()

        // default to Monday as first day of week
        weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

        // respond to the user tapping on the day steps progress circles
        //  (essentially making the day steps progress circles a button)
        view.dayCard.setOnClickListener { _ ->
            val theSteps = paseoDBHelper.getDaysSteps(SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("days", targetSteps)
            val averageSteps = paseoDBHelper.getAverageSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val maxSteps = paseoDBHelper.getMaxSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val theHour = SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()

            // fill in all the step details in the details bottom sheet
            val bottomSheet = DashboardStepsDetailsFragment()

            bottomSheet.timeUnit = getString(R.string.day)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetSteps * max(theHour, 1) / 24
            bottomSheet.projectedSteps = theSteps * 24 / max(theHour, 1)
            bottomSheet.targetSteps = targetSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the week steps progress circles
        //  (essentially making the week steps progress circles a button)
        view.weekCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(),0, weekStart)
            val onTarget = paseoDBHelper.getOnTarget("weeks", targetWeekSteps, weekStart)
            val maxSteps = paseoDBHelper.getMaxSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val minSteps = paseoDBHelper.getMinSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val averageSteps = paseoDBHelper.getAverageSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val theDayOfWeek = getDayOfWeekNumber(Date())

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.week)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for_week_of) + SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(getFirstDayInWeek())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetWeekSteps * theDayOfWeek / 7
            bottomSheet.projectedSteps = theSteps * 7 / theDayOfWeek
            bottomSheet.targetSteps = targetWeekSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the month steps progress circles
        //  (essentially making the month steps progress circles a button)
        view.monthCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("months", targetMonthSteps)
            val maxSteps = paseoDBHelper.getMaxSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val theDayOfMonth = SimpleDateFormat("d", Locale.getDefault()).format(Date()).toInt()

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.month)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("MMMM", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetMonthSteps * theDayOfMonth / daysInMonth
            bottomSheet.projectedSteps = theSteps * daysInMonth / theDayOfMonth
            bottomSheet.targetSteps = targetMonthSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the year steps progress circles
        //  (essentially making the year steps progress circles a button)
        view.yearCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("years", targetYearSteps)
            val maxSteps = paseoDBHelper.getMaxSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val minSteps = paseoDBHelper.getMinSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            val daysInYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
            val theDayOfYear = calendar.get(Calendar.DAY_OF_YEAR) // the day of the year in numerical format

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.year)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("yyyy", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetYearSteps * theDayOfYear / daysInYear
            bottomSheet.projectedSteps = theSteps * daysInYear / theDayOfYear
            bottomSheet.targetSteps = targetYearSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        return view
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()

        updateDashboard()
    }



    override fun onDestroy() {
        super.onDestroy()

        context?.unregisterReceiver(receiver)
    }



    // update the data displayed in the dashboard
    fun updateDashboard()
    {
        if (running)
        {
            // get the application settings
            val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
            targetSteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()

            // default to Monday as first day of week
            weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

            var dateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()) // looks like "19891225"
            val date = dateFormat.format(Date()).toInt()
            dateFormat = SimpleDateFormat("HH", Locale.getDefault())
            val hour = dateFormat.format(Date()).toInt()

            dateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
            dateValue.text = dateFormat.format(Date())

            // get today's steps from the steps table in the database
            var theSteps : Int = paseoDBHelper.getDaysSteps(date)

            // display today's steps on the main screen
            stepsValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            stepsProgressBar.max = targetSteps
            stepsProgressBar.progress = theSteps
            stepsProgressBar.secondaryProgress = targetSteps
            var theText = (theSteps.toDouble() / targetSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetSteps) + ")"
            percentTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            expectedStepsProgressBar.max = 24
            expectedStepsProgressBar.progress = hour

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedStepsProgressBar.max = targetSteps
            try {
                projectedStepsProgressBar.progress = min(theSteps * 24 / max(hour, 1), targetSteps)
            }
            catch (e: SQLiteException){

            }

            if (weekStart == 2) {
                dateWeekValue.text = getString(R.string.week_title)
            }
            else {
                dateWeekValue.text = getString(R.string.week_title2)
            }

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("weeks", date, 0 ,weekStart)

            // display today's steps on the main screen
            stepsWeekValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            stepsWeekProgressBar.max = targetWeekSteps
            stepsWeekProgressBar.progress = theSteps
            stepsWeekProgressBar.secondaryProgress = targetWeekSteps
            theText = (theSteps.toDouble() / targetWeekSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetWeekSteps) + ")"
            percentWeekTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            val theDayOfWeek = getDayOfWeekNumber(Date())

            expectedWeekStepsProgressBar.max = 7
            expectedWeekStepsProgressBar.progress = theDayOfWeek

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedWeekStepsProgressBar.max = targetWeekSteps
            try {
                projectedWeekStepsProgressBar.progress = min(theSteps * 7 / theDayOfWeek, targetWeekSteps)
            }
            catch (e: SQLiteException){
            }


            dateFormat = SimpleDateFormat("MMMM", Locale.getDefault()) // looks like "Dec 25, 1989

            theText = dateFormat.format(Date())
            dateMonthValue.text = theText

            // get this month's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("months", date)

            // display this month's steps on the main screen
            stepsMonthValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            stepsMonthProgressBar.max = targetMonthSteps
            stepsMonthProgressBar.progress = theSteps
            stepsMonthProgressBar.secondaryProgress = targetMonthSteps
            theText = (theSteps.toDouble() / targetMonthSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetMonthSteps) + ")"
            percentMonthTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            dateFormat = SimpleDateFormat("d", Locale.getDefault())
            val dayOfMonth = dateFormat.format(Date()).toInt()
            expectedMonthStepsProgressBar.max = daysInMonth
            expectedMonthStepsProgressBar.progress = dayOfMonth

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedMonthStepsProgressBar.max = targetMonthSteps
            try {
                projectedMonthStepsProgressBar.progress = min(theSteps * daysInMonth / dayOfMonth, targetMonthSteps)
            }
            catch (e: SQLiteException){
            }


            dateFormat = SimpleDateFormat("yyyy", Locale.getDefault()) // looks like "1989"
            dateYearValue.text = dateFormat.format(Date())

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("years", date)

            // display today's steps on the main screen
            stepsYearValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            stepsYearProgressBar.max = targetYearSteps
            stepsYearProgressBar.progress = theSteps
            stepsYearProgressBar.secondaryProgress = targetYearSteps
            theText = (theSteps.toDouble() / targetYearSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetYearSteps) + ")"
            percentYearTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            dateFormat = SimpleDateFormat("D", Locale.getDefault())
            val dayOfYear = dateFormat.format(Date()).toInt()
            expectedYearStepsProgressBar.max = daysInYear
            expectedYearStepsProgressBar.progress = dayOfYear

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            projectedYearStepsProgressBar.max = targetYearSteps
            try {
                projectedYearStepsProgressBar.progress = min(theSteps * daysInYear / dayOfYear, targetYearSteps)
            }
            catch (e: SQLiteException){
            }
        }
    }



    // determine the number of days in the month
    fun getDaysInMonth(theYear:Int, theMonth:Int = 0, theDay:Int = 0) : Int {
        val theCalendar = Calendar.getInstance()

        // use today's date if -1 is passed as the year
        if(theYear == -1){
            theCalendar.time = Date()
        }
        else {
            theCalendar.set(theYear, theMonth, theDay)
        }

        return theCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    }



    // get the day of week number of the first day of week based on the currently set Locale of the device
    fun getLocaleFirstDayOfWeek(): Int {
        val theCalendar = Calendar.getInstance(Locale.getDefault())
        return theCalendar.firstDayOfWeek
    }



    // return the first day of the week for the day that is passed to the function
    fun getFirstDayInWeek(): Date {

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // default to Monday as first day of week
        weekStart = paseoPrefs!!.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()
        var cal = Calendar.getInstance(Locale.UK)

        // use a CANADA calendar so that start of week is Sunday
        if (weekStart != 2) {
            cal = Calendar.getInstance(Locale.CANADA)
        }

        cal[Calendar.HOUR_OF_DAY] = 0
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)

        // get start of this week in milliseconds
        cal[Calendar.DAY_OF_WEEK] = cal.firstDayOfWeek

        return cal.time
    }



    // return the day of week number of the date passed to the function
    fun getDayOfWeekNumber(date: Date?): Int {

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        weekStart = paseoPrefs!!.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

        // default to Sunday as first day of week
        val cal = Calendar.getInstance()
        cal.time = date!!
        var dayOfWeek = cal[Calendar.DAY_OF_WEEK]

        // set first day of week to Monday if set by user (in app settings)
        if (weekStart == 2) {
            dayOfWeek = dayOfWeek - 1

            // when user has set week start on Monday and the Locale week start is Sunday,
            //   dayOfWeek for Sundays end up as day 0 (no good - division by zero later)
            //  need to set Sunday to day 7 (day of rest - maybe user should not take any steps at all)
            if (dayOfWeek < 1) {
                dayOfWeek = 7
            }
        }

        return dayOfWeek
    }


    // determine if the year passed to the function is a leap year
    fun isLeapYear(theYear: Int): Boolean {

        return when {
            theYear % 4 == 0 -> {
                when {
                    theYear % 100 == 0 -> theYear % 400 == 0
                    else -> true
                }
            }
            else -> false
        }
    }
}


